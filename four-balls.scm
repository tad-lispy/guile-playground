;; Open this file in libfive-studio

(set-bounds! [-10 -10 -10] [10 10 10])
(set-quality! 10)
(set-resolution! 20)

(define (four-balls radius distance blending)
  (blend 
    (blend 
      (sphere radius [distance distance 0])
      (sphere radius [(- distance) distance 0]) blending)
    (blend 
      (sphere radius [distance (- distance) 0])
      (sphere radius [(- distance) (- distance) 0]) blending) blending))

(four-balls 1 3 2.5)
