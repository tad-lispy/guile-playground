(define-module (my-namespace my-module)
  #:export (my-square my-value))

(define my-square (lambda (x) (* x x)))
(define my-value 7)
