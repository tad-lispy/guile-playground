{
  description = "Playing with Guile Scheme";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs }:
    let
      project_name = "guile-playground";
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
      };
    in {
      devShells.${system}.default = pkgs.mkShell {
        name = "${project_name}-development-shell";
        packages =  [
          pkgs.guile
          (pkgs.gnumake.override { guileSupport = true; })
          pkgs.ao # Old name for LibFive
          pkgs.cachix
          pkgs.jq
        ];
        QT_QPA_PLATFORM = "wayland;xcb"; # For LibFive to work under Wayland
        inherit project_name;
      };
    };
}
